import renderFilter from "./Classes/Filter.js";
import "./logic/cardFunctions/deleteCard.js"
import {loginModal, showModal, closeModal} from "./logic/modalFunctions/modal.js";
import {checkLocalStorage} from "./logic/authorization/authorization.js";
import {searchUrgency, searchStatus, searchInput} from "./logic/cardFunctions/filterCards.js";
import {auth} from "./logic/authorization/authorization.js";
import {getInfo} from "./logic/cardFunctions/getInfoFromModal.js";
import "./logic/cardFunctions/editCard.js"
import "./logic/cardFunctions/showMoreInfo.js"


checkLocalStorage()
closeModal()
// збір інформації з форми
const formCreateVisit = document.getElementById("our_modal")
formCreateVisit.addEventListener("submit", getInfo)
//логін
auth()
// відображаємо модалку логіну
loginModal()

// відображаємо модалку створення картки
showModal()

renderFilter()



//Фільтр
const selectUrgency = document.getElementById('search-urgency')
selectUrgency.addEventListener('change', searchUrgency)
const selectStatus = document.getElementById('search-status')
selectStatus.addEventListener('change', searchStatus);
searchInput()


