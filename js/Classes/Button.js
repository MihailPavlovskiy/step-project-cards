export default class Button {
    constructor(classList, id, text, type) {
        this.classList = classList
        this.id = id
        this.text = text
        this.type = type
    }
    createButton() {
        const btn = document.createElement('button')
        btn.classList.add(this.classList)
        btn.id = this.id
        btn.textContent = this.text
        btn.setAttribute("type", this.type);
        return btn
    }
}