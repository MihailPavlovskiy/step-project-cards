export const token = '6b162e35-667b-4ac6-a2bc-f51524229daf';
import Input from "./Input.js";
import Select from "./Select.js";
import Textarea from "./Textarea.js";

export default class Visit {
    renderVisit() {
        const bodyModal = document.createElement('fieldset')
        bodyModal.classList.add('modal_body')
        this.named = new Input(
            'name',
            'input_name_modal',
            'text',
            'select_modal',
            "Enter your name",
        )
        this.doctor = new Select(
            ['Choose doctor', 'Cardiologist', 'Dentist', 'Therapist'],
            'doctor',
            'select_doctor',
            () => {},
            'select_modal',
            ''
        )

        this.urgency = new Select(
            ['Urgency', 'Low', 'Normal', 'High'],
            'urgency',
            'select_urgency',
            () => {},
            'select_modal',
            ''
        )
        this.status = new Select(
            ['Status', 'Open', 'Done'],
            'statusCard',
            'select_status',
            () => {},
            'select_modal',
            ''
        )
        this.purpose = new Input(
            'aimVisit',
            'purpose',
            'text',
            'select_modal',
            'Visit purpose',
        )
        this.description = new Textarea(
            'description',
            'desc_modal',
            'select_modal',
            'Description',
        )
        bodyModal.append(
            this.named.renderInput(),
            this.doctor.renderSelect(),
            this.urgency.renderSelect(),
            this.status.renderSelect(),
            this.purpose.renderInput(),
            this.description.renderTextarea()
        )
        this.doctor.hideFirstOption();
        this.urgency.hideFirstOption();
        return bodyModal
    }
}
