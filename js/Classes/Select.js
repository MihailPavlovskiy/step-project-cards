function createElement(elemType, classNames = [], text) {
    const element = document.createElement(elemType)
    if (text) {
        element.textContent = text
    }
    element.classList.add(classNames)
    return element
}

export default class Select {
    constructor(options = [], name, id, handlerChange, className = ['form-control'], value = '') {
        this.options = options;
        this.name = name;
        this.id = id;
        this.className = className;
        this.value = value;
        this.handlerChange = handlerChange;
    }
    renderSelect() {
        this.select = createElement('select', this.className)
        this.select.setAttribute('id', this.id)
        this.select.name = this.name
        this.options.forEach(el => {
            this.option = createElement('option', ['option'], el)
            this.option.value = el
            this.select.appendChild(this.option)
        })
        this.select.addEventListener('change', this.handlerChange)
        return this.select
    }
    hideFirstOption() {
        let selectList = [...document.getElementsByTagName('select')];
        selectList.forEach(element => {
            let index;
            for (index = 0; index < element.length; index++) {
                if (index === 0) {
                    element[0].setAttribute('style', 'display:none;')
                }
            }
        })
    }
}