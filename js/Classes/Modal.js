import Visit from "./Visit.js";

export class Modal {
    renderFormModal() {
        const modal = document.getElementById('our_modal');
        const overlay = document.createElement('div');
        const form = document.createElement('form');
        form.setAttribute('type', 'submit');
        const extra = document.createElement('fieldset');
        extra.classList.add('extra');
        extra.id = 'extra';
        const headerModal = document.createElement('div');
        overlay.classList.add('overlay_modal');
        overlay.dataset.close = 'true'
        form.classList.add('modal_window');
        form.id = 'form';
        headerModal.classList.add('modal_header');
        headerModal.insertAdjacentHTML('beforeend', `<span class="modal_close" data-close="true">&times;</span>`);
        form.prepend(headerModal);
        form.append(extra);
        modal.append(overlay);
        overlay.append(form);
        return form
    }
    createModalVisit() {
        const extra = document.getElementById('extra');
        const showForm = new Visit();
        const firstModalVisit = showForm.renderVisit();
        form.insertBefore(firstModalVisit, extra);
        return form
    }
    open(){
        const openModal = document.getElementById('our_modal');
        openModal.classList.add('open');
    }
    close(){
        const closeModal = document.getElementById('our_modal');
        closeModal.classList.remove('open');
        document.body.style.overflow = 'scroll';
    }
}

function callModal() {
    const createModal = new Modal();
    createModal.renderFormModal();
    createModal.createModalVisit();
    createModal.open();
}
export { callModal }