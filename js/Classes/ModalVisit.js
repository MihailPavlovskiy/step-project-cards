import VisitDentist from "./VisitDentist.js";
import VisitTherap from "./VisitTherap.js";
import VisitCardio from "./VisitCardio.js";

class ModalVisit{
    constructor() {

    }
    renderFormDoctor(){
        const form = document.getElementById('form');
        const extra = document.getElementById('extra');

        const dateCardio = new VisitCardio();
        const fieldsetCardio = dateCardio.renderFormCardio();

        const dateDentist = new VisitDentist();
        const fieldsetDentist = dateDentist.renderFormDentist();

        const dateTherap = new VisitTherap();
        const fieldsetTherap = dateTherap.renderFormTherap();

        form.addEventListener('change', function (event){
            if (event.target.value === 'Cardiologist') {
                const fDoctor = document.querySelector('.fieldset_doctor');
                if(fDoctor){
                    fDoctor.remove()
                }
                extra.append(fieldsetCardio)
            } else if (event.target.value === 'Dentist'){
                const fDoctor = document.querySelector('.fieldset_doctor');
                if (fDoctor){
                    fDoctor.remove()
                }
                extra.append(fieldsetDentist)
            } else if (event.target.value === 'Therapist'){
                const fDoctor = document.querySelector('.fieldset_doctor');
                if (fDoctor){
                    fDoctor.remove()
                }
                extra.append(fieldsetTherap)
            }
        })
    }
}

function showDoctor () {
    const modal = new ModalVisit()
    modal.renderFormDoctor()
}

export {showDoctor}