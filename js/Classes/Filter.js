import Input from "./Input.js";
import Select from "./Select.js";

class Filter {
    constructor() {

    }

    render(){
        const filterDiv = document.querySelector('#wrapper_filter');
        this.inputSearch = new Input(
            'search-input',
            'search',
            'text',
            'search',
            'Напишіть щось'
        );
        this.selectUrgency = new Select(
            ['Urgency', 'All', 'High', 'Normal', 'Low'],
            'urgency',
            'search-urgency',
            () => {
            },
            ['form-control']
        )
        this.selectStatus = new Select(
            ['Status', 'All', 'Open', 'Done'],
            'status',
            'search-status',
        )
        filterDiv.append(
            this.inputSearch.renderInput(),
            this.selectUrgency.renderSelect(),
            this.selectStatus.renderSelect()
        )
        this.selectUrgency.hideFirstOption();
        this.selectStatus.hideFirstOption();
    }
}

function renderFilter() {
    const filter = new Filter()
    filter.render()
}

export default renderFilter;