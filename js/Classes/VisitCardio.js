import Button from "./Button.js";
import Input from "./Input.js";

export default class VisitCardio{
    constructor() {

    }

    renderFormCardio(){
        const fieldset = document.createElement('fieldset')
        fieldset.classList.add('fieldset_doctor')
        fieldset.id = 'fieldset_doctor'
        const normalTreasure = new Input('pressure', 'pressure_input', 'text', 'select_modal', 'Pressure').renderInput();
        const bodyIndex = new Input('bmi', 'BMI_input', 'text', 'select_modal', 'BMI').renderInput();
        const illnesses = new Input('disease', 'disease_input', 'text', 'select_modal','Disease patient').renderInput();
        const age = new Input('age', 'age_input', 'text', 'select_modal', 'Age').renderInput();
        const button = new Button('btn_c', 'create_new_card', 'Create card', 'submit').createButton();
        fieldset.append(normalTreasure, bodyIndex, illnesses, age, button);
        return fieldset
    }
}