import Button from "./Button.js";
import Input from "./Input.js";
import {Modal} from "./Modal.js";

class ModalLogin extends Modal{

    renderFormLogin(){
        const form = this.renderFormModal()
        const extra = document.getElementById('extra');
        const pEmail = document.createElement('p');
        pEmail.textContent = 'Email';
        pEmail.classList.add('p_mod');
        const pPassword = document.createElement('p');
        pPassword.textContent = 'Password';
        pPassword.classList.add('p_mod');

        const inputForEmail = new Input('email', 'input_email', 'email', 'input_em', 'Введіть ваш email').renderInput();
        const inputForPassword = new Input('password', 'input_password', 'password', 'input_pass',  'Введіть ваш пароль').renderInput();

        const buttonForLogin = new Button('btn_l', 'btn_login', 'Login', 'submit').createButton();

        extra.append(pEmail, inputForEmail, pPassword, inputForPassword, buttonForLogin);
    }
}

function showModalLogin () {
    const modalLogin = new ModalLogin();
    modalLogin.renderFormLogin();
    modalLogin.open();
}

export {showModalLogin}