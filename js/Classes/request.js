import instance from "../logic/router/instance.js"

const Api_Token = '6b162e35-667b-4ac6-a2bc-f51524229daf'
export default class Request {
    constructor(password = '0000331111', email = 'admin123@gmail.com', token = Api_Token) {
        this.password = password
        this.email = email
        this.token = token
    }

    async createCard(dataBody) {
        const {status, data} = await instance.post(' ', dataBody, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.token}`,
            }
        })
        return {status, data}
    }

    async getAllCards() {
        const {data} = await instance.get(' ', {
            headers: {Authorization: `Bearer ${this.token}`}
        })

        return data
    }

    async getCard(id) {
        const {data} = await instance.get(`${id}`, {
            headers: {Authorization: `Bearer ${this.token}`}
        })
        console.log(data)
        return data
    }

    async deleteCard(id) {
        let stat
        await instance
            .delete(`${id}`, {
                headers: {Authorization: `Bearer ${this.token}`}
            })
            .then(({status}) => {
                stat = status
            })
        return stat
    }

    async editCard(id, dataBody) {
        const {status, data} = await instance.put(`${id}`, dataBody, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.token}`,
            }
        })
        return {status, data}
    }

    async checkCards() {
        const {data} = await instance.get(' ', {
            headers: {
                Authorization: `Bearer ${this.token}`,
            }
        })
        return data.length
    }

    async getTokenForAuth(body) {
        const {data, status} = await instance.post('https://ajax.test-danit.com/api/v2/cards/login', body)
            .catch(err => {
                alert('Неправильный пароль')
            })
        return {data, status}
    }
}