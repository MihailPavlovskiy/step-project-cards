import {Modal, callModal} from "../../Classes/Modal.js";
import {showDoctor} from "../../Classes/ModalVisit.js";
import {showModalLogin} from "../../Classes/ModalLogin.js";
import {info} from "../cardFunctions/getInfoFromModal.js";

function closeModal() {
    const modal = document.getElementById('our_modal')
    modal.addEventListener('click', event => {
        if (event.target.dataset.close) {
            const close = new Modal()
            close.close()
        }
    })
}

function loginModal() {
    const buttonLogin = document.getElementById('button_sign')
    buttonLogin.addEventListener('click', event => {
        const overlay = document.querySelector('.overlay_modal')
        if (overlay) {
            overlay.remove()
        }
        showModalLogin()
    });
}

function showModal() {
    const bntModalForm = document.getElementById('button_create')
    bntModalForm.addEventListener('click', () => {
        document.body.style.overflow = ('hidden')
        const overlay = document.querySelector('.overlay_modal')
        if (overlay) {
            overlay.remove()
        }
        callModal()
        showDoctor()
        info()
    });
}



export {closeModal, showModal, loginModal};