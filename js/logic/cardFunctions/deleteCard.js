import Request from "../../Classes/request.js"
import {parent} from "./cardsWrapper.js";
import {isCardOnPage} from "./isCardOnPage.js";

const delCard = new Request()
const bannerDelCard = document.createElement("div")
bannerDelCard.classList.add("banner-del-card", "hide")
bannerDelCard.innerText = "Картка була видалена успішно!"
document.body.append(bannerDelCard)
parent.addEventListener("click", async(event) => {
    if (event.target.classList.contains("btn-delete")) {
        let id = event.target.getAttribute("type")
        let stat = await delCard.deleteCard(id)
        if (stat === 200) {
            event.target.closest(".card").remove()
            bannerDelCard.classList.remove("hide")
            setTimeout(() => {
                bannerDelCard.classList.add("hide")
            }, 1000)
        }
    }
    isCardOnPage()
});