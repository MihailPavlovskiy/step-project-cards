import {Modal} from "../../Classes/Modal.js";
import Request from "../../Classes/request.js";
import {cardsRender} from "./cardsRender.js";
import {isCardOnPage} from "./isCardOnPage.js";

function info() {
    const fields = document.querySelectorAll("input, select, textarea")
    const values = {}
    fields.forEach(elem => {
        if (elem.closest('#form')) {
            const { name, value } = elem
            values[name] = value
        }
    })
    return values
}

async function getInfo(event) {
    event.preventDefault()
    let infoNewCard
    const addNewCard = new Request()
    const val = info()
    const { id, ...rest } = val

    if (id === null || id === undefined || id === '') {
        infoNewCard = await addNewCard.createCard(rest)
    } else {
        infoNewCard = await addNewCard.editCard(id, rest)
    }

    if (infoNewCard.status === 200) {
        const data = []
        data[0] = infoNewCard.data
        cardsRender(data)
        isCardOnPage()
        document.body.style.overflow = 'scroll'
        const close = new Modal()
        close.close()
    }
}

export {info}
export {getInfo}