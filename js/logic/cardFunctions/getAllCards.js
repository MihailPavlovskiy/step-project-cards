import Request from "../../Classes/request.js"
import { cardsRender } from "./cardsRender.js"
import {isCardOnPage} from "./isCardOnPage.js";

async function getAllCards() {
    const getAllCardsToRender = new Request()
    const allCardsToRender = await getAllCardsToRender.getAllCards()
    cardsRender(allCardsToRender)
    isCardOnPage()
}

export { getAllCards }