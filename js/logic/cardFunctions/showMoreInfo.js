import {parent} from "./cardsWrapper.js";

parent.addEventListener("click", showMoreInfo);

function showMoreInfo(event) {
    if (event.target.classList.contains("btn-show-more")) {
        const currentCard = event.target.closest('.card')
        const collapsedDiv = currentCard.querySelector('.card__more-info')
        if (collapsedDiv.classList.contains('collapsed')) {
            event.target.textContent = 'Close more info'
            collapsedDiv.classList.remove('collapsed')
        } else {
            event.target.textContent = 'Show more...'
            collapsedDiv.classList.add('collapsed')
        }
    }
}