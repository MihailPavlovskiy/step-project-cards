import Request from "../../Classes/request.js"
import VisitCardio from "../../Classes/VisitCardio.js";
import VisitTherap from "../../Classes/VisitTherap.js";
import VisitDentist from "../../Classes/VisitDentist.js";
import {Modal} from "../../Classes/Modal.js";
import {callModal} from "../../Classes/Modal.js";
import { showDoctor } from "../../Classes/ModalVisit.js";
import {parent} from "./cardsWrapper.js";
import { cardsRender } from "./cardsRender.js";
import {isCardOnPage} from "./isCardOnPage.js";
import { getInfo } from "./getInfoFromModal.js";


let idCard

const formCreateVisit = document.getElementById("our_modal")

function information() {
    const fields = document.querySelectorAll("input, select, textarea")
    const values = {}
    fields.forEach(elem => {
        if (elem.closest('#form')) {
            const { name, value } = elem
            values[name] = value
        }
    })
    values.id = idCard
    return values
}
async function repeatInfo(event) {
    event.preventDefault()
    let infoNewCard
    const addNewCard = new Request()
    const val = information()
    const { id, ...rest } = val
    infoNewCard = await addNewCard.editCard(id, rest)

    if (infoNewCard.status === 200) {
        const data = []
        data[0] = infoNewCard.data
        cardsRender(data)
        isCardOnPage()
        document.body.style.overflow = 'scroll'
        const close = new Modal()
        close.close()
    }
}




parent.addEventListener("click", async(event) => {
    if (event.target.classList.contains("btn-edit")) {
        formCreateVisit.removeEventListener('submit', getInfo)
        formCreateVisit.addEventListener('submit', repeatInfo)

        const cardForEdit = event.target.closest('.card')

        const valesFields = getInfoFromCard(cardForEdit)
        idCard = valesFields.id

        document.body.style.overflow = ('hidden')
        const overlay = document.querySelector('.overlay_modal')
        if (overlay) {
            overlay.remove()
        }
        callModal()
        showDoctor()

        const formVisit = document.querySelector('#form')

        const dateCardiologist = new VisitCardio()
        const fieldsetCardiologist = dateCardiologist.renderFormCardio()

        const dateDentist = new VisitDentist()
        const fieldsetDentist = dateDentist.renderFormDentist()

        const dateTherapeutic = new VisitTherap()
        const fieldsetTherapeutic = dateTherapeutic.renderFormTherap()

        const extra = document.getElementById('extra')

        if (valesFields.doctor === 'Cardiologist') {
            extra.append(fieldsetCardiologist)
        } else if (valesFields.doctor === 'Therapist') {
            extra.append(fieldsetTherapeutic)
        } else if (valesFields.doctor === 'Dentist') {
            extra.append(fieldsetDentist)
        }

        for (let key in valesFields) {
            let fild = formVisit.querySelector(`[name="${key}"]`)
            try {
                fild.value = valesFields[key]
            } catch (error) {

            }
        }
        cardForEdit.remove()
    }
});

function getInfoFromCard(cardForEdit) {
    const values = {}
    const cardFields = cardForEdit.querySelectorAll('span, p')
    cardFields.forEach(elem => {
        let key = elem.dataset.item
        let value = elem.textContent
        values[key] = value;
    })
    return values
}

export { idCard }